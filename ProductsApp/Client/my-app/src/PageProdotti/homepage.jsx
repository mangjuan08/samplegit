import React, { Component } from "react";
import axios from "axios";
import ModalAddProduct from "./modalAddProduct";
import { API_URL, API_URL_CATEG, API_DELETE_URL } from "../strings/urls";
import { style, style2, style3, style4 } from "../strings/css";
import 'react-notifications/lib/notifications.css';
import { NotificationContainer, NotificationManager } from 'react-notifications';
import Moment from 'react-moment';

class HomePage extends Component {
  _isMounted = false;
  constructor(props) {
    super(props);

    this.state = {
      Product: [],
      IdProduct: 0,
      NameProduct: "",
      TotalProducts: 0,
      DescProd: "",
      NumbersOfProd: 0,
      GetValue: false,
      refreshListProd: false,
      isClickTheProduct: false,
      Categories: [],
      IdCategory: 0,
      NameCategory: "",
      CategorySelected: "",
      CategorySelectedId: 0,
      ImageFile: "",
      ImageUrl: "",
      DatePosted: ""
    };
    this.controller = new AbortController();
  }
  componentDidMount() {
    this._isMounted = true;
    console.log("THIS IS " + this._isMounted)
    this.fetchList();
    this.fetchCategory();
    /*this.SetId();*/

  }

  unfetchlist = () => {
    this.setState({
      Product: [],
      TotalProducts: 0
    });
  };

  fetchList = () => {
    fetch(API_URL)
      .then(result => result.json())
      .then(data => {
        if (this._isMounted) {
          this.RenderListProduct(data)
        } else {
          this._isMounted = false
        }
      })
  };

  fetchCategory = () => {
    fetch(API_URL_CATEG)
      .then(response => response.json())
      .then(data => this.RenderCategoryList(data));
  };

  RenderCategoryList(data) {
    for (var i = 0; i < data.length; i++) {
      this.state.Categories.push(data[i]);

      this.setState({
        IdCategory: data[i].IdCategory,
        NameCategory: data[i].NameCategory
      });
    }
  }

  RenderListProduct(data) {
    for (var i = 0; i < data.length; i++) {
      //pushing in an array
      this.state.Product.push({
        IdProduct: data[i].IdProduct,
        NameProduct: data[i].NameProduct,
        DescProd: data[i].DescProd,
        NumbersOfProd: data[i].NumbersOfProd,
        ImageFile: new Buffer(data[i].ImageFile).toString('binary'),
        imageURL: new Buffer(data[i].ImageFile).toString('binary'),
        DatePosted: data[i].DatePosted
      }
      );
      this.setState({
        IdProduct: data[i].IdProduct,
        NameProduct: data[i].NameProduct,
        DescProd: data[i].DescProd,
        NumbersOfProd: data[i].NumbersOfProd,
        TotalProducts: data.length,
        DatePosted: data[i].DatePosted
      });
    }
  }

  GetProductValue = (Name, Id, Dates, Total, Desc, ImageFile) => {
    this.setState({
      NameProduct: Name,
      IdProduct: Id,
      DatePosted: Dates,
      NumbersOfProd: Total,
      DescProd: Desc,
      ImageFile: ImageFile,
      GetValue: true,
      isClickTheProduct: true
    });
  };

  DeleteProduct = e => {
    const id = e.target.id;
    axios.delete(API_DELETE_URL + id).then(res => this.EmptyArray());
    NotificationManager.warning('Warning message', 'Close after 3000ms', 3000);
  };

  EmptyArray = e => {
    this.setState({
      Product: [],
      refreshListProd: true,
      isClickTheProduct: false
    });

    this.fetchList();
  };

  SelectedCategory = (e) => {

    let index = e.target.selectedIndex;
    let el = e.target.childNodes[index]
    let optionId = el.getAttribute('id');

    this.setState({
      CategorySelected: e.target.value,
    })
    /*
    const ReturnResult = this.state.Categories.filter(function (items) {
      return items.IdCategory == optionId;
    });*/

    this.setState({
      CategorySelectedId: optionId
    }, () => {

      this.SetId(this.state.CategorySelectedId)
    })
    /*console.log(ReturnResult[0].NameCategory)
    console.log(ReturnResult[0].IdCategory)*/
  }

  SetId(id) {
    this.setState({
      CategorySelectedId: id
    })
    console.log("ID OF SELECTED CATEGORY: " + this.state.CategorySelectedId + " CATEGORY:" + this.state.CategorySelected)
  }



  componentWillUnmount() {
    this._isMounted = false;
    console.log("TRY " + this._isMounted)
    /*get the first selected category
    this.SelectedCategory();*/
  }


  render() {
    return (
      <div className="container" style={style4}>
        <div className="row">
          <div className="col-md-6">
            <h2>ACTUAL CATEGORY:
            {this.state.CategorySelected === "" ? "" : this.state.CategorySelected}
            </h2>
          </div>
          <div className="col-md-2">
            &nbsp;
          </div>
          <div className="col-md-4">
            <div className="input-group mb-2 row">
              <div className="input-group-prepend">
                <h3><strong>CATEGORY</strong></h3>
              </div>
              <select className="form-control custom-select col-12" value={this.state.CategorySelected} onChange={this.SelectedCategory}>
                {this.state.Categories.map(items => (
                  <option key={items.IdCategory} name="tipocat" value={items.NameCategory} id={items.IdCategory}>{items.NameCategory}</option>
                ))}
              </select>
            </div>
          </div>
        </div>
        <hr></hr>
        <div className="row">
          <div className="col-md-12">
            <br></br>
            <div className="card">
              <div className="card-header">
                <br></br>
                <div className="input-group mb-2 row">
                  <div className="input-group-prepend">
                    <button
                      className="btn btn-primary btn-sm"
                      onClick={this.EmptyArray}
                    >
                      <i className="fas fa-sync-alt"></i>
                    </button>
                    <ModalAddProduct RefetchList={this.EmptyArray} />
                    <strong>
                      <h4>PRODUCT LISTS{" "}
                        <span className="badge badge-primary badge-pill">
                          {this.state.TotalProducts}
                        </span></h4>
                    </strong>
                  </div>
                </div>
              </div>
              <div className="card-body">
                <ul style={style3} >
                  {/*show a list of Products*/
                    this.state.Product.map(items => (
                      <li
                        key={items.IdProduct}
                        style={style}
                        onClick={this.GetProductValue.bind(
                          this,
                          items.NameProduct,
                          items.IdProduct,
                          items.DatePosted,
                          items.NumbersOfProd,
                          items.DescProd,
                          items.ImageFile
                        )}
                      >
                        <img src={items.ImageFile} width="50" alt="img"></img>
                        <strong>{items.NameProduct}</strong>
                        <br></br>
                        <small>INSERTED: <Moment>{items.DatePosted}</Moment></small>
                        <br></br>
                        <small>TOTAL COUNT: {items.NumbersOfProd}</small>

                        <button
                          className="btn btn-danger btn-light close"
                          onClick={this.DeleteProduct}
                          id={items.IdProduct}
                          style={style2}
                          aria-label="Close"
                        >
                          x
                      </button>
                        <hr></hr>
                      </li>
                    ))}
                </ul>
              </div>
            </div>
          </div>
          <NotificationContainer />
        </div>
        <hr></hr>
        <br></br>
      </div>
    );
  }
}
export default HomePage;
{/*
          <div className="col-md-8">
            <br></br>
            <br></br>
            <Orders RefetchListProduct={this.EmptyArray} />
            <br></br>
          </div>*/}