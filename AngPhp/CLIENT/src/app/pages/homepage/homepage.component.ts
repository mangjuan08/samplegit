import { Component, OnInit } from '@angular/core';
import { UtentiService } from 'src/app/services/utenti.service';
import { NgForm } from '@angular/forms';
import { TokenService } from 'src/app/services/token.service';
import { Router } from '@angular/router';




@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})
export class HomepageComponent implements OnInit {

  constructor(private router:Router,private service: UtentiService, private token: TokenService) { }

  ngOnInit() {
    this.resetForm();
  }

  onSubmit(form: NgForm) {
    this.service.LoginUser().subscribe(
      (response) => {
        
     console.log(response);
        //set the token by calling a function of a service TokenService
        this.token.HandleToken(response);
        

        //navigate to user page
        this.router.navigate(['/User']);

      },
    (err) => {
      console.log(err);
    }
    );


  }



  resetForm(form?: NgForm) {
    if (form != null)
      form.resetForm();
    this.service.resetFormLogin();
  }
}
