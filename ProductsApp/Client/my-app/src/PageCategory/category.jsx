import React, { Component } from 'react';
import { API_URL_CATEG } from "../strings/urls";
import { style4 } from "../strings/css";




class Category extends Component {
    _isMounted = false;
    constructor() {
        super();
        this.state = {
            Categories: [],
            IdCategory: 0,
            NameCategory: "",
        }
    }

    componentDidMount() {
        this._isMounted = true;
        this.fetchCategory();
    }

    fetchCategory = () => {
        fetch(API_URL_CATEG)
            .then(response => response.json())
            .then(data => this.RenderCategoryList(data));
    };

    RenderCategoryList(data) {
        for (var i = 0; i < data.length; i++) {
            this.state.Categories.push(data[i]);

            this.setState({
                IdCategory: data[i].IdCategory,
                NameCategory: data[i].NameCategory
            });
        }
        console.log(this.state.Categories)
    }
    componentWillUnmount() {
        this._isMounted = false;
    }
    render() {
        return (
            <div className="container" style={style4}>
                <h1>CATEGORY</h1>
                <ul>
                    {
                        this.state.Categories.map((items) => (
                            <li key={items.IdCategory} name="category">{items.NameCategory}</li>
                        ))
                    }
                </ul>
            </div>
        );
    }

}

export default Category;