import React, { Component } from 'react';
import {
    BrowserRouter as Router,
    Switch,
    Route,
    Link
} from "react-router-dom";
import HomePage from '../PageProdotti/homepage';
import Category from '../PageCategory/category';
import '../strings/index.css';
import OrdersClient from '../PageOrders/OrdersClient';
class MainPage extends Component {
    _isMounted = false

    componentDidMount() {
        this._isMounted = true
    }

    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {
        return (
            <Router>

                <nav className="navbar navbar-expand-lg navbar-light bg-light">
                    <div className="container">
                        <Link to="/" className="navbar-brand"><img src="http://www.aubay.it/aubay/wp-content/uploads/2017/05/cropped-logo_aubay_RGB_with-halo.png" width="50" alt="img"></img></Link>
                        <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                            <span className="navbar-toggler-icon"></span>
                        </button>

                        <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
                            <div className="navbar-nav ml-auto">
                                <Link to="/category" className="nav-item nav-link" >CATEGORY</Link>
                                <Link to="/orders" className="nav-item nav-link">ORDERS</Link>
                            </div>
                        </div>
                    </div>

                </nav>

                <Switch>
                    {/* <AnimatedSwitch
                        atEnter={{ opacity: 0 }}
                        atLeave={{ opacity: 0 }}
                        atActive={{ opacity: 1 }}
                        className="switch-wrapper"
                    >*/}

                    <Route path="/category/" component={Category}>
                    </Route>
                    <Route path="/orders/" component={OrdersClient}>
                    </Route>
                    <Route path="/" component={HomePage}>
                    </Route>
                    {/* </AnimatedSwitch>*/}
                </Switch>

            </Router>
        );
    }
}
export default MainPage;
