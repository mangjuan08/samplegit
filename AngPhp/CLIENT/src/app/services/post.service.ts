import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  constructor(private http:HttpClient) { }
  readonly url = "http://localhost:8000/api";
  
  getAll() {
    return this.http.get(this.url+"/post");
  }
}
