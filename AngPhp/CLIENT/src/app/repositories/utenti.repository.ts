import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Login } from '../datamodel/login.datamodel';

@Injectable()

export class UtentiRepository {

    constructor(private http: HttpClient) { }
    readonly url = "http://localhost:8000/api";
    
    getAllData() {
        return this.http.get(this.url + "/utenti").toPromise();
    }


    LoginUser(login: Login) {
        console.log(login);
        return this.http.post(this.url + "/login", login);
    }
    

}