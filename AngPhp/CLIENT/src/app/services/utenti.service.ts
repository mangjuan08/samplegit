import { Injectable } from '@angular/core';
import { UtentiRepository } from '../repositories/utenti.repository';
import { Utenti } from '../datamodel/utenti.datamodel';
import { Login } from '../datamodel/login.datamodel';



@Injectable({
  providedIn: 'root'
})
export class UtentiService {

  utenti: Utenti[]
  login: Login;
  constructor(private utentiRepo: UtentiRepository) { }



  getAllUsers() {
    this.utentiRepo.getAllData().then(
      res => this.utenti = res as Utenti[]
    )
  }


  getDetailUser() {

  }

  LoginUser() {
    return this.utentiRepo.LoginUser(this.login);
  }

 

  resetFormLogin() {
    this.login = {
      email: '',
      password: ''
    }
  }
}
