import { Component, OnInit } from '@angular/core';
import { TokenService } from 'src/app/services/token.service';
import { UtentiService } from 'src/app/services/utenti.service';

@Component({
  selector: 'app-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.css']
})
export class UserPageComponent implements OnInit {

  constructor(private userService:UtentiService,private token:TokenService) { }

  ngOnInit() {
  }
  Logout() {
    this.token.RemoveToken();
  }
}
