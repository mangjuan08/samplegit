import React, { Component } from "react";

class UserLoggedIn extends Component {
  constructor(props) {
    super(props);
    this.state = {
      tokens: this.props.token
    };
    this.Logout = this.props.Logout.bind(this);

    this.getToken = this.getToken.bind(this);
  }

  getToken() {
    console.log(this.state.tokens);
  } /*
  logout() {
    localStorage.removeItem("token");
    this.props.history.push("/");
  }*/
  render() {
    return (
      <div className="container">
        <h1>user page</h1>
        <button onClick={this.Logout}>logout</button>
        <br></br>
        <button onClick={this.getToken}>getToken</button>
      </div>
    );
  }
}

export default UserLoggedIn;
