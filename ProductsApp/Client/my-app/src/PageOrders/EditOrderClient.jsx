import React, { Component } from 'react';

class EditOrderClient extends Component {
    constructor(props) {
        super(props);
        this.state = {
            IdPro: this.props.IdProduct,
            IdCli: this.props.IdClient,
            Quant: '',
        }
    }
    /*LOL */
    render() {
        return (
            <div>
                <div className="modal-dialog" role="document">
                    <div className="modal-content">
                        <div className="modal-header">
                            <h5 className="modal-title" id="exampleModalLabel">Modal title</h5>
                            <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div className="modal-body">
                            <p>ID PRODUCT: {this.props.IdProduct}</p> <br></br>
                            <p>ID CLIENT: {this.props.IdClient}</p> <br></br>
                            <p>PREVIOIS VALUE: {this.props.PreviousValueQuantity}</p>
                            <form onSubmit={this.props.UpdateOrder}>
                                <input value={this.props.isShow === true ? this.props.Quantita : ""} name="Quantita" onChange={this.props.HandleEditOrder}></input>
                                <input type="hidden" value={this.props.isShow === true ? this.props.IdProduct : ""} name="IdProduct" ></input>
                                <input type="hidden" value={this.props.isShow === true ? this.props.IdClient : ""} name="IdClient" ></input>
                                <br></br>
                                <br></br>
                                <button type="submit" className="btn btn-primary" aria-label="Close" >UPDATE</button>
                            </form>
                        </div>
                        <div className="modal-footer">
                            <button type="button" className="btn btn-secondary" data-dismiss="modal" aria-label="Close" onClick={this.props.EmptyArray}>Close</button>
                            <button type="button" className="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}

export default EditOrderClient;


