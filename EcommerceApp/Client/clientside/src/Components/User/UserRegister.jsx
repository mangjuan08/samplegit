import React, { Component } from "react";
import axios from "axios";
import { UserReg } from "../../url/urls";
import "../../css/DragAndDrop.css";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";

class UserRegister extends Component {
  constructor(props) {
    super(props);
    this.state = {
      UserDetails: [],
      Email: "",
      Password: "",
      Name: "",
      Surname: "",
      ImageFile: "",
      ImageName: "",
      ImageType: "",
      ExistImage: false
    };
  }

  HandleInputs = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };

  HandleSubmit = e => {
    axios
      .post(UserReg, {
        Email: this.state.Email,
        Password: this.state.Password,
        Surname: this.state.Surname,
        Name: this.state.Name,
        ImageFile: this.state.ImageFile
      })
      .then(res => console.log(res));
    e.preventDefault();
    this.props.history.push("/Login");
  };

  HandlePassword = e => {
    console.log(e.target.value);
  };

  handleFile = e => {
    /*console.log(e.target.files[0]);*/
    const files = e.target.files[0];
    if (files.size <= 3000000 && files.type === "image/jpeg") {
      let data = new FormData();
      data.append("file", files);

      let reader = new FileReader();
      reader.onloadend = e => {
        this.setState({
          ImageFile: e.target.result,
          ImageName: files.name,
          ImageType: files.type,
          ExistImage: true
        });
      };
      reader.readAsDataURL(files);
      alert("Success");
    } else {
      document.getElementById("fileInput").value = "";
      alert("IMAGE IS not valid");
    }
  };

  ClearImage = e => {
    this.setState({
      ImageFile: "",
      ImageName: ""
    });
    document.getElementById("fileInput").value = "";
  };
  /*payload resolve in the server*/
  render() {
    return (
      <div className="container">
        <br></br>
        <br></br>
        <br></br>
        <form onSubmit={this.HandleSubmit}>
          <div className="form-row">
            <div className="form-group col-md-4">
              <TextField
                label="Email"
                name="Email"
                onBlur={this.HandleInputs}
                fullWidth
                InputLabelProps={{
                  shrink: true
                }}
              />
            </div>
            <div className="form-group col-md-4">
              <TextField
                label="Password"
                type="password"
                onChange={this.HandleInputs}
                name="Password"
                fullWidth
                InputLabelProps={{
                  shrink: true
                }}
                onBlur={this.HandlePassword}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="form-group col-md-4">
              <TextField
                label="Name"
                name="Name"
                onBlur={this.HandleInputs}
                fullWidth
                InputLabelProps={{
                  shrink: true
                }}
              />
            </div>
            <div className="form-group col-md-4">
              <TextField
                label="Surname"
                name="Surname"
                onBlur={this.HandleInputs}
                fullWidth
                InputLabelProps={{
                  shrink: true
                }}
              />
            </div>
          </div>
          <div className="form-row">
            <div className="col-md-8">
              <br></br>
              <div className="form-group files">
                <input
                  type="file"
                  id="fileInput"
                  name="filess"
                  className="form-control"
                  onChange={this.handleFile}
                ></input>
              </div>
              <button
                type="button"
                className="btn btn-danger btn-sm"
                onClick={this.ClearImage}
              >
                CLEAR
              </button>
            </div>
          </div>
          <Button type="submit" variant="outlined" color="primary">
            Register
          </Button>
        </form>
        <br></br>
      </div>
    );
  }
}

export default UserRegister;
