import React, { Component } from 'react';
import axios from 'axios'
import { API_URL_ADD_PRODUCT } from '../strings/urls'
import '../strings/lol.css';
import 'react-notifications/lib/notifications.css';
import {NotificationManager} from 'react-notifications';

class ModalAddProduct extends Component {
    constructor(props) {
        super(props);
        this.state = {
            Product: [],
            NameProduct: '',
            DescProd: '',
            NumbersOfProd: 0,
            DatePosted: '',
            ImageFile: "",
            ImageName: "",
            ImageType: "",
            ExistImage: false,
            PrezzoProdotto: 0,
            InputKey: ""
        }
        this.RefetchList = this.props.RefetchList.bind(this)
    }
    HandleInputs = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }
    AddProduct = (e) => {
        const arrayProduct = Object.assign([], this.state.Product);
      
        arrayProduct.push({
            NameProduct: this.state.NameProduct,
            DescProd: this.state.DescProd,
            NumbersOfProd: this.state.NumbersOfProd,
            ImageFile: this.state.ImageFile,
            PrezzoProdotto: this.state.PrezzoProdotto
        })
        this.setState({
            state: arrayProduct
        })
        axios.post(API_URL_ADD_PRODUCT, this.state)
            .then(res => this.RefetchList)
        this.setState({
            ImageFile: ""
        })
        NotificationManager.success('Success message', 'Title here',3000);
        document.getElementById("form").reset();
        e.preventDefault();
    }

    handleFile = e => {
        console.log(e.target.files[0])
        const files = e.target.files[0]
        if (files.size <= 300000 && files.type === "image/jpeg") {
            let data = new FormData()
            data.append('file', files)

            let reader = new FileReader();
            reader.onloadend = (e) => {
                this.setState({
                    ImageFile: e.target.result,
                    ImageName: files.name,
                    ImageType: files.type,
                    ExistImage: true
                });
            }
            reader.readAsDataURL(files);
            alert("Success");
        }
        else {
            document.getElementById("fileInput").value="";
            alert("IMAGE IS not valid")
        }
    }

    ClearImage = (e) => {
        this.setState({
            ImageFile: "",
            ImageName: ""
        })
        document.getElementById("fileInput").value = ""
    }
    render() {
        return (
            <div>
                <button className="btn btn-success btn-sm" data-toggle="modal" data-target="#exampleModal" style={{ float: "right" }}>
                    <i className="fas fa-plus" ></i>
                </button>
                <div className="modal fade" id="exampleModal" tabIndex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div className="modal-dialog" role="document">
                        <div className="modal-content">
                            <div className="modal-header">
                                <h5 className="modal-title" id="exampleModalLabel">ADD PRODUCT</h5>
                                <button type="button" className="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                            <div className="modal-body">
                                <form onSubmit={this.AddProduct} id="form">
                                    <div className="form-row">
                                        <div className="col">
                                            <input type="text" className="form-control" placeholder="Product Name" name="NameProduct" onBlur={this.HandleInputs}></input>
                                        </div>
                                        <div className="col">
                                            <input type="text" className="form-control" placeholder="Description" name="DescProd" onBlur={this.HandleInputs}></input>
                                        </div>
                                        <div className="col">
                                            <input type="text" className="form-control" placeholder="Quantity" name="NumbersOfProd" onBlur={this.HandleInputs}></input>
                                        </div>
                                    </div><br></br>
                                    <div className="form-row">
                                        <div className="col">
                                            <input type="text" className="form-control" placeholder="Prezzo Prodotto" name="PrezzoProdotto" onBlur={this.HandleInputs}></input>
                                        </div>
                                    </div>
                                    <br></br>
                                    <div className="form-row">
                                        <div className="col">
                                            <br></br>
                                            <div className="form-group files">
                                                <input type="file" id="fileInput" name="filess" className="form-control" onChange={this.handleFile}></input>
                                            </div>
                                            <button type="button" className="btn btn-danger btn-sm" onClick={this.ClearImage}>CLEAR</button>
                                        </div>
                                    </div>
                                    <br></br>
                                    <button className="btn btn-primary btn-block btn-sm" type="submit">ADD</button>
                                </form>
                            </div>
                            <div className="modal-footer">
                                <button type="button" className="btn btn-danger" onClick={this.props.RefetchList} data-dismiss="modal">REFRESH LIST & CLOSE</button>
                            </div>
             
                        </div>
                    </div>
                </div>
            </div >
        );
    }
}
export default ModalAddProduct;


/*
import DropToUpload from 'react-drop-to-upload';
import { style5 } from '../strings/css';
<br></br>
                                    <div className="form-row">
                                        <div className="col">
                                            <DropToUpload
                                                onDrop={this.handleDrop} style={style5}
                                                id="DropTo">DROP HERE
                                            </DropToUpload>
                                            <img src={this.state.ImageFile}></img>

                                        </div>

                                    </div>

                                    */


                                    /*
    handleDrop = (e) => {
        console.log(e.target.files[0])

        /*console.log(files[0].name)*/
        /*
        if (files[0].size <= 26000 && files[0].type === "image/jpeg") {
            let data = new FormData()
            data.append('file', files)

            let reader = new FileReader();
            reader.onloadend = (e) => {
                this.setState({
                    ImageFile: e.target.result,
                    ImageName: files[0].name,
                    ImageType: files[0].type,
                    ExistImage: true
                });

            }

           
            reader.readAsDataURL(files[0]);

            alert("Success");
        }
        else {
            alert("IMAGE IS not valid")
        }
    }*/