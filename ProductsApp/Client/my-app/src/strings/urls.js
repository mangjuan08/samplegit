/* PRODUCTS */

export const API_URL = "http://localhost:9000/api/products"

export const API_DELETE_URL = "http://localhost:9000/api/products/delete/";

export const API_URL_ADD_PRODUCT = "http://localhost:9000/api/products/addproduct"


/*ORDERS*/

export const API_URL_ORDERS = "http://localhost:9000/api/orders";
export const API_URL_ORDERS_HOME = "http://localhost:9000/api/orders/home";
export const API_URL_DELETE_ORDER = "http://localhost:9000/api/orders/deleteOrder";
export const API_URL_INSERT_ORDER = "http://localhost:9000/api/orders/insertOrder"
export const API_URL_UPDATE_ORDER = "http://localhost:9000/api/orders/updateOrder"

/*CATEGORY*/

export const API_URL_CATEG = "http://localhost:9000/api/category";