export const style = {
    marginBottom: "10px",
    cursor: "pointer",
    position: "relative"
};

export const style2 = {
    position: "absolute",
    top: "0",
    right: "0"
};

export const style3 = {

    listStyle: "none",
    marginLeft: "-30px"

}


export const style4 = {
    marginTop: "60px"
}

export const style5 = {

    background: "#efefef",
    display: "flex",
    alignItems: "center",
    justifyContent: "center",
    width: "460px",
    height: "80px",
    border: "solid 40px transparent",
    transition: "all 250ms ease-in-out 0s",
    position: "relative"

}


export const style6 = {

    background: "transparent",
    display: "block",
    fontFamily: "Arial",
    color: "black",
    fontSize: "10px",
    fontWeight: "bold",
    textAlign: "center",
    transition: "all 250ms ease-in-out 0s"

}