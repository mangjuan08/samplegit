import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";
import axios from "axios";
import Button from "@material-ui/core/Button";
import { Link } from "react-router-dom";
import { Login } from "../../url/urls";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";

class UserLogin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      Email: "",
      Password: "",
      isLoggedin: false,
      tokens: this.props.token,
      errors: this.props.error
    };
  }
  HandleInputs = e => {
    this.setState({
      [e.target.name]: e.target.value
    });
  };
  HandleSubmit(e) {
    axios
      .post(Login, {
        Password: this.state.Password,
        Email: this.state.Email
      })
      .then(response => {
        if (response.data.status === "success") {
          localStorage.setItem("token", response.data.token);
          console.log("Before" + this.state.tokens);
          this.setState(
            {
              isLoggedin: true,
              tokens: response.data.token
            },
            () => {
              console.log("after " + this.state.tokens);
              this.props.login(this.state.isLoggedin, this.state.tokens);
            }
          );
        } else {
          this.props.history.push("/Login");
        }
      });

    /*localStorage.setItem("token", response.data.token)*/
    e.preventDefault();
  }

  render() {
    return (
      <div className="container">
        <br></br>
        <br></br>
        <br></br>
        <form onSubmit={this.HandleSubmit.bind(this)}>
          <div className="form-row">
            <div className="form-group col-md-4">
              <TextField
                label="Email"
                name="Email"
                onBlur={this.HandleInputs}
                fullWidth
                InputLabelProps={{
                  shrink: true
                }}
              />
            </div>
            <div className="form-group col-md-4">
              <TextField
                label="Password"
                type="password"
                onBlur={this.HandleInputs}
                name="Password"
                fullWidth
                InputLabelProps={{
                  shrink: true
                }}
              />
            </div>
          </div>
          <Link to="/Register" style={{ fontSize: "13px" }}>
            <AccountCircleIcon></AccountCircleIcon>New User? Click here
          </Link>
          <br></br>
          <br></br>
          <Button type="submit" variant="outlined" color="primary">
            LOGIN
          </Button>
        </form>
        <p>{this.state.errors > 1 ? alert("error") : ""}</p>
      </div>
    );
  }
}

export default UserLogin;
