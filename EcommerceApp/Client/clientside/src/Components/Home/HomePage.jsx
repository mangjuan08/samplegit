import React, { Component } from 'react';
import '../../css/HomePage.css'
class HomePage extends Component {


    render() {
        return (
            <div className="container" id="container">
                <h1> SHOPSITE!</h1>
                <br></br>
                <div className="row">
                    <button className="btn btn-primary btn-sm" onClick={this.props.Add}>click</button>
                </div>
            </div>
        );
    }
}

export default HomePage;