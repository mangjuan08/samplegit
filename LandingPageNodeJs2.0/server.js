var express = require('express');
var app = express();
var body = require('body-parser');
var path = require('path');
var index = require('./routes/index');



//body parser
app.use(body.json());
app.use(body.urlencoded({ extended: true }));

//routing
app.use('/',index);

//setup view engine
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');



//listen  to the port 3000
app.listen(3000, function (err) {
    console.log("connected");
    if(err)
    throw err;
});

//for the images 
app.use(express.static("public"));







