import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
import { UtentiService } from './services/utenti.service';
import { PostService } from './services/post.service';
import { UtentiRepository } from './repositories/utenti.repository';

import { UserPageComponent } from './pages/user-page/user-page.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MaterialModule } from './material-module';
import { AdminPageComponent } from './pages/admin-page/admin-page.component';
import { NavbarComponent } from './extraComponents/navbar/navbar.component';
import { TokenService } from './services/token.service';
import { UserCardComponent } from './pages/user-page/user-card/user-card.component';


@NgModule({
  declarations: [
    AppComponent,
    HomepageComponent,
    UserPageComponent,
    AdminPageComponent,
    NavbarComponent,
    UserCardComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    MaterialModule,
    BrowserAnimationsModule
  ],
  providers: [UtentiService, PostService, TokenService, UtentiRepository],
  bootstrap: [AppComponent]
})
export class AppModule { }
