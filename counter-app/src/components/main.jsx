import React, { Component } from 'react';
import Counters from './counters';
import 'devextreme/dist/css/dx.common.css';
import 'devextreme/dist/css/dx.light.css';
import Chart, {
    ArgumentAxis,
    Series,
    Legend
} from 'devextreme-react/chart';

const data = [{
    arg: 1990,
    val: 5320816667
}, {
    arg: 2000,
    val: 6127700428
}, {
    arg: 2010,
    val: 6916183482
}];

class Main extends Component {

    render() {
        return (
            <div class="container">
                <br></br><br></br><br></br>
                <div class="row">

                    <div class="col-md-4">

                    </div>
                    <div class="col-md-4">
                        <Counters />
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <Chart dataSource={data}>
                            <ArgumentAxis tickInterval={10} />
                            <Series type="bar" />
                            <Legend visible={false} />
                        </Chart>
                    </div>
                </div>
            </div>

        )


    }


}


export default Main;