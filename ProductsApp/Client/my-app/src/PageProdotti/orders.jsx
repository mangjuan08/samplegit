import React, { Component } from "react";
import axios from "axios";
import EditOrder from "./editOrder";
import { API_URL_ORDERS, API_URL_DELETE_ORDER, API_URL_UPDATE_ORDER, API_URL_ORDERS_HOME } from '../strings/urls';
import { style4 } from "../strings/css";

class Orders extends Component {
  constructor(props) {
    super(props);
    this._isMounted = false;
    this.state = {
      Orders: [],
      NameProduct: "",
      NameClient: "",
      Stato: "",
      IdProduct: 0,
      Quantita: 0,
      PrezzoTotale: 0,
      TotalOrders: 0,
      CurrentDate: "",
      IdClient: 0,
      PreviousValueQuantity: 0,
      date: new Date()
    };
    /*this.RefetchList = this.props.RefetchListProduct.bind(this);*/

  }



  componentDidMount() {

    this._isMounted = true
    /*fetch(API_URL_ORDERS)
       .then(response => response.json())
       .then(data => this.RenderList(data));*/

    this.fetchOrderList();
    this.timerID = setInterval(
      () => this.tick(),
      1000
    );
    /*setInterval(() => {
      this.setState({
        CurrentDate: new Date().toLocaleString()
      });
    }, 100);*/
  }

  fetchOrderList = () => {
    fetch(API_URL_ORDERS_HOME)
      .then(response => response.json())
      .then(data => this.RenderList(data));

  };

  EmptyArray = (e) => {
    this.setState({
      Orders: [],
      Quantita: 0,
      IdClient: 0,
      IdProduct: 0,
      Stato: "",
      isShow: false
    });

    this.fetchOrderList();
  };


  unfetchOrderList = () => {
    this.setState({
      Orders: []
    });

    this.fetchOrderList();

  };

  RenderList(data) {
    for (var i = 0; i < data.length; i++) {
      this.state.Orders.push(data[i]);
      this.setState({
        NameProduct: data[i].NameProduct,
        Name: data[i].Name,
        Stato: data[i].Stato,
        IdProduct: data[i].IdProduct,
        Quantita: data[i].Quantita,
        PrezzoTotale: data[i].PrezzoTotale,
        IdClient: data[i].IdClient,
        TotalOrders: data.length
      });
    }
  }

  DeleteOrder = (IdProduct, IdClient, Quantita) => {
    axios
      .delete(
        API_URL_DELETE_ORDER + "/" + IdProduct + "/" + IdClient + "/" + Quantita,
        {
          params: {
            IdProduct: IdProduct,
            IdClient: IdClient,
            Quantita: Quantita
          }
        }
      )
      .then(res => this.unfetchOrderList());
    this.EmptyArray();

  };


  HandleEditOrder = (e) => {
    this.setState({
      [e.target.name]: e.target.value,
      PreviousValueQuantity: e.target.value
    })
  }

  UpdateOrder = (e) => {
    e.preventDefault()
    const Q = e.target.elements.Quantita.value
    const IdCli = e.target.elements.IdClient.value
    const IdPro = e.target.elements.IdProduct.value

    axios.put(API_URL_UPDATE_ORDER + "/" + Q + "/" + IdPro + "/" + IdCli, {
      params: {
        IdProduct: IdPro,
        Quantita: Q,
        IdClient: IdCli
      }
    })
      .then(response =>
        console.log(response))

    this.EmptyArray();
  }

  getItems = (Prod, Client, Quantity) => {
    this.setState({
      IdProduct: Prod,
      IdClient: Client,
      Quantita: Quantity,
      isShow: true
    })
  }
  tick() {
    this.setState({
      date: new Date()
    });
  }
  componentWillUnmount() {

    this._isMounted = false;

    clearInterval(this.timerID);

  }

  render() {
    return (

      <div className="card">
        <div className="card-header">
          <strong>ORDER LIST: {this.state.TotalOrders}</strong> <button className="btn btn-light text-primary btn-sm" onClick={this.EmptyArray}>
            <i className="fas fa-sync-alt"></i>
          </button>
          <p><strong>{this.state.date.toLocaleTimeString()}</strong></p>
        </div>
        <div className="card-body">
          <table className="table table-bordered table-hover">
            <thead className="thead-light">
              <tr>
                <th scope="col">
                  <i className="fas fa-list"></i> PRODUCT NAME
                  </th>
                <th scope="col">
                  <i className="fas fa-portrait"></i> CLIENT
                  </th>
                <th scope="col">
                  <i className="fas fa-sort-numeric-up-alt"></i> N.
                  ORDERS
                  </th>
                <th scope="col">STATO</th>
                <th scope="col">
                  <i className="fas fa-euro-sign"></i> TOTALE PREZZO
                  </th>
                <th scope="col">DELETE/EDIT</th>
              </tr>
            </thead>
            <tbody>
              {this.state.Orders.map(items => (
                <tr key={items.IdProduct + items.IdClient} onClick={this.getItems.bind(this, items.IdProduct, items.IdClient, items.Quantita)}>
                  <td>{items.NameProduct}</td>
                  <td>{items.Name}</td>
                  <td>{items.Quantita}</td>
                  <td>
                    <strong>{items.Stato}</strong>
                  </td>
                  <td>
                    <strong>€ {items.Quantita * items.PrezzoProdotto} </strong>
                  </td>
                  <td>
                    <button
                      className="btn btn-danger"
                      onClick={this.DeleteOrder.bind(
                        this,
                        items.IdProduct,
                        items.IdClient,
                        items.Quantita
                      )}
                      disabled={items.Stato === "CONSEGNA" ? true : false}
                    >
                      <i className="far fa-trash-alt"></i>
                    </button>
                    <button type="button" className="btn btn-success" data-toggle="modal" data-target="#EditOrder">
                      <i className="far fa-edit"></i>
                    </button>
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
          <div className="modal fade" id="EditOrder" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <EditOrder isShow={this.state.isShow}
              IdProduct={this.state.IdProduct}
              IdClient={this.state.IdClient}
              Quantita={this.state.Quantita}
              EmptyArray={this.EmptyArray}
              HandleEditOrder={this.HandleEditOrder}
              UpdateOrder={this.UpdateOrder}
            />
          </div>
        </div>
      </div>

    );
  }
}

export default Orders;
