const express = require('express');
const Route = express.Router();
var bp = require("body-parser");
var cors = require("cors");
var db = require("../db/connect");

/*FETCH ALL ORDERS*/
Route.get('/', cors(), function(req, res) {
    db.query(
        "SELECT COUNT(*) as totalCount, IdClient,Name,Surname,IdOrder,Stato,Quantita,DateOrder,NameCategory,NameProduct,PrezzoProdotto FROM orders cp,products p,clients c,categoria ca WHERE ca.IdCategory = p.FkIdCategory AND cp.FkIdProduct = p.IdProduct AND c.IdClient=cp.FkIdClient GROUP BY IdOrder,NameProduct,DateOrder ORDER BY IdOrder",
        function(req, results) {
            const a = JSON.stringify(results);
            res.send(a);
        }
    );
})

Route.get('/home', cors(), function(req, res) {
    db.query(
        "SELECT * FROM orders cp,products p,clients c,categoria ca WHERE ca.IdCategory = p.FkIdCategory AND cp.FkIdProduct = p.IdProduct AND c.IdClient=cp.FkIdClient",
        function(req, results) {
            const a = JSON.stringify(results);
            res.send(a);
        }
    );
})

Route.put("/updateOrder/:Quantita/:IdProduct/:IdClient", cors(), function(req, res) {
    const Quantita = req.params.Quantita;
    const IdProd = req.params.IdProduct;
    const IdClient = req.params.IdClient;
    let query =
        "CALL UpdateOrders (" + IdProd + "," + Quantita + "," + IdClient + ")";
    db.query(query, (error, results) => {
        if (error) console.log(error);
    });
    res.send("success")
})


module.exports = Route;