import React, { Component } from 'react'
import Card from './card'
import NewFormContact from './newformcontact';
import EditContact from './editcontact';
import './../index.css';

const style = {
    height: "100%",
    width: "100%",
    overflow: 'hidden',
    overflowY: 'scroll',
    border: "1px solid rgba(0, 0, 0, 0.125)",
    padding: "30px",
    position: 'absolute',
    listStyle: "none",
    backgroundColor: "#212529"
}

class Showcontactdetail extends Component {

    /*constructor is equal to function = (e) => {}*/
    constructor(props) {
        super(props);
        this.state = {
            user: [
                { firstname: 'Contact 1', lastname: 'a', numero: 3215678907 },
                { firstname: 'Contact 2', lastname: 'b', numero: 3212315643 },
                { firstname: 'Contact 3', lastname: 'c', numero: 3253212313 },
                { firstname: 'Contact 4', lastname: 'd', numero: 3331234212 }
            ],
            firstname: '',
            lastname: '',
            numero: null,
            Name: '',
            showValue: false,
            disabledInput: true,
            buttonFinishEdit: true,
            buttonEdit: false

        }
    }


    ClickContact = (e) => {

        this.setState({

            firstname: e.target.value,
            lastname: e.target.name,
            numero: e.target.id
        })
    }

    ClearCard = () => {

        this.setState({
            firstname: '',
            lastname: '',
            numero: null,
            showValue: false
        })

        document.getElementById("form").reset();
    }


    /*update contact*/
    UpdateContact = (e) => {

        e.preventDefault();
        var a = this.state.user.find((element) => {
            return element.numero === this.state.numero


        })
        console.log(a)


    }


    //delete a contact
    DeleteContact = (index) => {

        const arrayUser = Object.assign([], this.state.user);
        arrayUser.splice(index, 1);
        this.setState({
            user: arrayUser
        })
        this.ClearCard();

    }


    HandleEditContact = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    HandleNewFormContact = (e) => {
        this.setState({
            [e.target.name]: e.target.value
        })
    }

    AddContact = (e) => {
        e.preventDefault();
        const arrayUser = Object.assign([], this.state.user);
        arrayUser.push({
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            numero: this.state.numero
        })

        this.setState({
            user: arrayUser
        })

        alert("SUCCESS");
        this.ClearCard();

    }
    //onBlur is an event in which it gets a string that is inserted by a user

    FinishEdit = () => {
        this.setState({
            showValue: false,
            disabledInput: true,
            buttonEdit: false,
            buttonFinishEdit: true
        })
        this.ClearCard();
    }


    EditContact = () => {
        if (this.state.firstname === '' && this.state.lastname === '' && this.state.numero === null) {
            alert("NOTHING TO EDIT");
        }
        else {
            this.setState({
                showValue: true,
                disabledInput: false,
                buttonFinishEdit: false,
                buttonEdit: true
            })
        }

    }

    render() {
        console.log("ARRAY LENGTH:" + this.state.user.length)

        return (

            <div className="container">
                <br></br>
                <h1>CONTACT APP</h1>

                <hr></hr>

                <div className="row">
                    <div className="col-md-4">

                        <ul style={style}>
                            <p><strong>TOTAL CONTACT LIST:{this.state.user.length}</strong></p>
                            {
                                /*show a list of contacts*/
                                this.state.user.map((items, index) =>

                                    <li key={items.numero} style={{ marginBottom: "23px" }} >

                                        <button style={{ width: "267px" }} className="btn btn-primary"
                                            onClick={this.ClickContact} value={items.firstname}
                                            id={items.numero}
                                            name={items.lastname}
                                        >
                                            {items.firstname}
                                        </button>
                                        <button className="btn btn-danger rounded-circle"
                                            onClick={this.DeleteContact.bind(this, index)}>x</button>
                                    </li>
                                )
                            }
                        </ul>
                    </div>
                    <div className="col-md-8">
                        <div className="card bg-dark text-white">
                            <Card
                                firstname={this.state.firstname}
                                lastname={this.state.lastname}
                                numero={this.state.numero}
                                id={this.state.id}
                                clearAll={this.ClearCard}
                                EditCon={this.EditContact}
                                FinishEdit={this.FinishEdit}
                                buttonFinishEdit={this.state.buttonFinishEdit}
                                buttonEdit={this.state.buttonEdit}
                            />
                        </div>
                    </div>
                </div>
                <hr></hr>
                <div className="row">
                    <div className="col-md-6">
                        <NewFormContact
                            firstname={this.state.firstname}
                            submit={this.AddContact}
                            HandleNewFormContact={this.HandleNewFormContact} />
                    </div>
                    <div className="col-md-6">
                        <EditContact showValue={this.state.showValue}
                            numero={this.state.numero}
                            lastname={this.state.lastname}
                            firstname={this.state.firstname}
                            UpdateContact={this.UpdateContact}
                            Edit={this.edit}
                            disabledInput={this.state.disabledInput}
                            HandleEditContact={this.HandleEditContact}
                        />
                    </div>
                </div>
                <hr></hr>

            </div>
        )

    }
}

export default Showcontactdetail;

