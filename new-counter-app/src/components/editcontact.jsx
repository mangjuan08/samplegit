import React, { Component } from 'react'


class EditContact extends Component {

    /*
    onBlur={this.Check2}
    onBlur={this.Check3}
value={this.props.showValue ? this.props.lastname : " "}
value={this.props.showValue ? this.props.numero : " "} 
    */


    render() {

        return (
            <div className="card bg-dark text-white">
                <div className="card-header"><strong>EDIT CONTACT</strong></div>
                <div className="card-body">
                    <form onSubmit={this.props.UpdateContact}>
                        <div className="form-group">
                            {/*if the edit button is clicked it will show the value and assigned in this input*/}
                            <input type="text" className="form-control" name="firstname" value={this.props.showValue ? this.props.firstname : ""} placeholder="Insert FirstName" onBlur={this.props.HandleEditContact} disabled={this.props.disabledInput}></input>
                        </div>

                        <div className="form-group">
                            <input type="text" className="form-control" name="lastname" value={this.props.showValue ? this.props.lastname : ""} placeholder="Insert Lastname" onBlur={this.props.HandleEditContact} disabled={this.props.disabledInput}></input>
                        </div>

                        <div className="form-group">
                            <input type="text" className="form-control" name="numero" value={this.props.showValue ? this.props.numero : ""} placeholder="Insert Number" onBlur={this.props.HandleEditContact} disabled={this.props.disabledInput}></input>
                        </div>

                        <button type="submit" className="btn btn-primary" disabled={this.props.disabledInput}>UPDATE CONTACT</button>

                    </form>
                </div>
            </div>
        )

    }
}

export default EditContact;