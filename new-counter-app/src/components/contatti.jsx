import React, { Component } from 'react'
import Showcontactdetail from './showcontactdetail'


class Contatti extends Component {

    state = {
        user: [
            { firstname: 'lol', lastname: 'a', id: 1 },
            { firstname: 'lol2', lastname: 'a', id: 2 },
            { firstname: 'lol3', lastname: 'a', id: 3 },
            { firstname: 'lol4', lastname: 'a', id: 4 }
        ]
    }

    prova = (e) => {
        /*console.log(e.target.value);
        console.log(e.target.id);*/
        /*[e.target.firstname]: e.target.value;*/
        this.setState({
            firstname: e.target.value
        })



        /*this.props.onChange(val);*/
    }

    render() {

        return (
            <React.Fragment>
                <h1>CONTATTI</h1>
                <ul>
                    {
                        this.state.user.map((items) =>
                            <li key={items.id}>
                                <Showcontactdetail firstname={this.state.user.firstname} />
                                <button class="btn btn-primary" onClick={this.prova} value={items.firstname} id={items.id}>{items.firstname}</button>
                            </li>
                        )
                    }
                </ul>

            </React.Fragment>

        )

    }
}

export default Contatti;