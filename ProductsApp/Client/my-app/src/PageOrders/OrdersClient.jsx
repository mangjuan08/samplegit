import React, { Component } from "react";
import { API_URL_ORDERS, API_URL_DELETE_ORDER, API_URL_INSERT_ORDER } from '../strings/urls';
import 'devextreme/data/odata/store';
import { Column, DataGrid, FilterRow, HeaderFilter, GroupPanel, Scrolling, Editing, Grouping, Lookup, MasterDetail, Summary, RangeRule, RequiredRule, StringLengthRule, GroupItem, TotalItem, ValueFormat } from 'devextreme-react/data-grid';
import { createStore } from 'devextreme-aspnet-data-nojquery';


const dataSource = createStore({
    key: 'IdOrder',
    requireTotalCount: true,
    loadUrl: API_URL_ORDERS,
    deleteUrl: API_URL_DELETE_ORDER,
    insertUrl: API_URL_INSERT_ORDER,
    onBeforeSend: (method, ajaxOptions) => {
        ajaxOptions.xhrFields = { withCredentials: false };
    }

});

class OrdersClient extends Component {
    constructor(props) {
        super(props);
        this._isMounted = false;
        this.state = {
            Orders: [],
            NameProduct: "",
            NameClient: "",
            Stato: "",
            IdProduct: 0,
            Quantita: 0,
            PrezzoTotale: 0,
            TotalOrders: 0,
            CurrentDate: "",
            IdClient: 0,
            PreviousValueQuantity: 0,
            date: new Date()
        };
    }



    componentDidMount() {
        this._isMounted = true
    }


    componentWillUnmount() {
        this._isMounted = false;
    }

    render() {

        return (
            <div className="container">
                <br></br>
                <DataGrid
                    dataSource={dataSource}
                    showBorders={true}
                    height={600}
                    remoteOperations={true}
                >
                    <FilterRow visible={true} />
                    <HeaderFilter visible={true} />
                    <GroupPanel visible={true} />

                    <Editing
                        mode="row"
                        allowAdding={true}
                        allowDeleting={true}
                        allowUpdating={true}
                    />
                    <Grouping autoExpandAll={false} />
                    <Column dataField="IdOrder">
                    </Column>
                    <Column dataField="Name">
                    </Column>
                    <Column dataField="Surname">
                    </Column>
                    <Column dataField="NameProduct">
                    </Column>
                    <Column dataField="NameCategory">
                    </Column>
                    <Column dataField="Quantita"></Column>
                    <Column dataField="DateOrder" dataType="Date">
                    </Column>
                </DataGrid></div>
        );
    }
}

export default OrdersClient;
