const express = require('express');
const Route = express.Router();
var bp = require("body-parser");
var cors = require("cors");
var db = require("../db/connect");
const moment = require('moment');
let now = moment();


/*FETCH ALL PRODUCTS*/
Route.get("/", cors(), function(req, res) {
    var FetchAllProducts = "SELECT *FROM `products`";
    db.query("SELECT * FROM `products`", function(req, results) {
        /*const a = JSON.stringify(results);*/
        res.send(results);
    });
});


Route.post("/addproduct", cors(), function(req, res) {
    //req.body is used for passing data that is not show in url
    /*let sql_insert = `INSERT INTO products(NameProduct,DescProd,DatePosted,NumbersOfProd,PrezzoProdotto,ImageFile)  VALUES ?  `;*/
    var date_inserted = new Date().toISOString().slice(0, 10) + " " + new Date().toISOString().slice(11, 19)
    var date_ins = now.format("YYYY-MM-DD")
    let parameter = [
        [
            req.body.NameProduct,
            req.body.DescProd,
            date_ins,
            req.body.NumbersOfProd,
            req.body.PrezzoProdotto,
            req.body.ImageFile
        ]
    ];
    db.query(`INSERT INTO products(NameProduct,DescProd,DatePosted,NumbersOfProd,PrezzoProdotto,ImageFile)  VALUES ? `, [parameter], function(reqs, results, err) {
        if (err) throw err;
        res.send(now.format("YYYY-MM-DD"));
    });
});


Route.delete("/delete/:id", cors(), function(req, res) {
    //req.params is used for parameters that are passed in url

    let { id } = req.params;
    let sql_delete = "DELETE  FROM products WHERE IdProduct = ?";
    let parameter = [
        [id]
    ];
    db.query(sql_delete, [parameter], function(reqs, results, err) {
        if (err) throw err;
        res.send("SUCCESS");
    });
});


module.exports = Route;