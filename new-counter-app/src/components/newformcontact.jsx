import React, { Component } from 'react'

const style = {
    marginLeft: "10px"
}

class NewFormContact extends Component {

    /*this.FunctionPassedClearingDetails = this.FunctionPassedClearingDetails.bind(this);*/
    constructor(props) {
        super(props)
        this.state = {

            firstname: '',
            lastname: '',
            numero: 0
        }
    }




    //clearAll is the function where is assigned the other function from another component(showcontactdetail)
    //the clear function from component showcontactdetail is assigned to the function clearAll

    resetForm = (e) => {
        document.getElementById("form").reset();


    }

    render() {

        return (
            <div className="card bg-dark text-white">
                <div className="card-header"><strong>ADD CONTACT</strong></div>
                <div className="card-body">
                    <form onSubmit={this.props.submit} id="form">
                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Insert FirstName" name="firstname" onBlur={this.props.HandleNewFormContact}></input>
                        </div>

                        <div className="form-group">
                            <input type="text" className="form-control" placeholder="Insert Lastname" name="lastname" onBlur={this.props.HandleNewFormContact}></input>
                        </div>

                        <div className="form-group">
                            <input type="number" className="form-control" placeholder="Insert Number" name="numero" onBlur={this.props.HandleNewFormContact}></input>
                        </div>

                        <button type="submit" className="btn btn-primary">ADD CONTACT</button>
                        <button type="button" className="btn btn-danger" onClick={this.resetForm} style={style}>CLEAR</button>

                    </form>

                </div>
            </div>
        )

    }
}

export default NewFormContact;