var express = require('express');
var app = express();
var body = require('body-parser');
const { check, validationResult } = require('express-validator');
var fs = require('fs');
var path = require('path');


const PDFDocument = require('pdfkit');


// create application/x-www-form-urlencoded parser

app.use(body.json());
app.use(body.urlencoded({ extended: true }));


app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


app.get('/', function (req, res) {
    res.render('index');
})

app.post('/sendData', [
    
    check('email', 'email is required').isEmail(),
    check('nome', 'name is required').not().isEmpty(),
    check('nome', 'letters only').isAscii(),
    check('cognome', 'surname is required').not().isEmpty(),
    check('cognome', 'letters only').isAscii(),
    check('numeroTelefono', 'number is required').not().isEmpty(),
    check('numeroTelefono', 'insert only number').isInt(),
    check('azienda', 'azienda is required').not().isEmpty(),
], function (req, res) {

    var prova = "LOL";
    var nome = req.body.nome;
    var cognome = req.body.cognome;
    var email = req.body.email;
    var numeroTelefono = req.body.numeroTelefono;
    var azienda = req.body.azienda;
    /*
    res.render('index', {
        error:false
    });*/
    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        console.log(errors);
        /* res.render('index', {
            error:false
        });*/

    }
    else {

        // Create a document
        const doc = new PDFDocument;


        // Pipe its output somewhere, like to a file or HTTP response
        // See below for browser usage
        doc.pipe(fs.createWriteStream('pdf/' + nome + '.pdf'));


        doc.moveDown();
        doc.text(`NOME: ${nome}`, {
            width: 410,
            align: 'center',

        }
        );

        doc.moveDown();
        doc.text(`COGNOME: ${cognome}`, {
            width: 410,
            align: 'center'
        }
        );

        


        // Finalize PDF file
        doc.end();



    }



    /*
    
    // Create a document
    const doc = new PDFDocument;
    
    
    // Pipe its output somewhere, like to a file or HTTP response
    // See below for browser usage
    doc.pipe(fs.createWriteStream('pdf/'+nome+'.pdf'));
    
    
    
    
    // Add another page
    doc.fontSize(25)
       .text(nome, 100, 100);
    
    // Finalize PDF file
    doc.end();
    
    
    
    
    
    
    
    
    */


















});


//listen  to the port 3000
app.listen(3000, function () {
    console.log("connected");
});










