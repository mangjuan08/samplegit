const express = require("express");
const Route = express.Router();
const bp = require("body-parser");
const cors = require("cors");
const db = require("../db/connect");
const crypto = require("crypto");
const multer = require("multer");
const upload = multer({ dest: "uploads/" });
const jwt = require("jsonwebtoken");
/*
Route.post("/login", cors(), function(req, res) {
  //req.body is used for passing data that is not show in url
  let sql_insert = `INSERT INTO products(NameProduct,DescProd,DatePosted,NumbersOfProd,PrezzoProdotto,ImageFile)  VALUES ?  `;
  const pass = req.body.Password;
  const email = req.body.Email;

  const user = {
    email: req.body.Email,
    pass: req.body.Password
  };
 const hash = crypto.createHash('sha256').update(pass).digest('base64');
        let parameter = [
            [
                req.body.Name,
                req.body.Surname,
                req.body.Email,
                hash
            ]
        ];
        db.query(`INSERT INTO users(Name,Surname,Email,Password)  VALUES ? `, [parameter], function(reqs, results, err) {
            if (err) throw err;
            res.send("success");
        });
  const data = JSON.stringify(user);
  const secret = "TOPSECRETTTTT";
  const now = Math.floor(Date.now() / 1000),
    iat = now - 10,
    expiresIn = 3600,
    expr = now + expiresIn,
    notBefore = now - 10,
    jwtId = Math.random()
      .toString(36)
      .substring(7);
  const payload = {
    iat: iat,
    jwtid: jwtId,
    audience: "TEST",
    data: data
  };
  jwt.sign(
    payload,
    secret,
    { algorithm: "HS256", expiresIn: expiresIn },
    function(err, token) {
      res.json({
        token: token,
        status: "success"
      });
    }
  );
});*/

Route.post("/register", cors(), upload.single("avatar"), function(req, res) {
  //req.body is used for passing data that is not show in url
  let sql_insert = `INSERT INTO products(NameProduct,DescProd,DatePosted,NumbersOfProd,PrezzoProdotto,ImageFile)  VALUES ?  `;
  const pass = req.body.Password;

  const hash = crypto
    .createHash("sha256")
    .update(pass)
    .digest("base64");

  let parameter = [[req.body.Name, req.body.Surname, req.body.Email, hash]];
  db.query(
    `INSERT INTO users(Name,Surname,Email,Password)  VALUES ? `,
    [parameter],
    function(reqs, results, err) {
      if (err) throw err;
      res.send("success");
    }
  );

  /*if (!image) {
    console.log("No file received");
    return res.send({
      success: false
    });
  }*/
});

Route.post("/login", cors(), function(req, res) {
  const user = {
    email: req.body.Email,
    pass: req.body.Password
  };
  //hash the password
  const hash = crypto
    .createHash("sha256")
    .update(user.pass)
    .digest("base64");

  const query = "SELECT *, COUNT(*) as total FROM users WHERE Email=?";

  db.query(query, req.body.Email, function(error, result) {
    var rows = JSON.parse(JSON.stringify(result[0]));

    //compare if record exists into 1
    if (rows.total == 1) {
      //compare the password found in db into the hash password
      if (rows.Password == hash) {
        var userDetails = JSON.stringify(user);
        jwt.sign(
          { userDetails },
          "this_secret_key",
          { expiresIn: "1h" },
          (errors, token) => {
            res.json({
              token: token,
              status: "success"
            });
          }
        );
      } else {
        //res.sednStatus(403)
        res.json({
          status: "failed"
        });
      }
    } else {
      //res.sednStatus(403)
      res.json({
        status: "failed"
      });
    }
  });
});

module.exports = Route;
