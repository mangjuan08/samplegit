import React, { Component } from 'react'


const style = {
    marginLeft: "10px"
}

class Card extends Component {

    /*this.FunctionPassedClearingDetails = this.FunctionPassedClearingDetails.bind(this);*/
    state = {

        firstname: this.props.firstname,
        lastname: this.props.lastname,
        numero: this.props.numero
    }






    //clearAll is the function where is assigned the other function from another component(showcontactdetail)
    //the clear function from component showcontactdetail is assigned to the function clearAll




    render() {

        return (<div>
            <div className="card-header"><strong>CONTACT: {this.props.firstname}</strong></div>
            <div className="card-body">
                <h4><strong>NAME:   {this.props.firstname} </strong></h4>
                <br></br><h4><strong>LASTNAME: {this.props.lastname}</strong></h4>
                <br></br><h4><strong>NUMERO: {this.props.numero}</strong></h4>

                <br></br><br></br>
                <button className="btn btn-danger" onClick={this.props.clearAll}>CLEAR</button>
                <button className="btn btn-success" style={style} onClick={this.props.EditCon} disabled={this.props.buttonEdit}>EDIT</button>
                <button className="btn btn-primary" style={style} onClick={this.props.FinishEdit} disabled={this.props.buttonFinishEdit}>FINISH EDITING</button>
            </div>
        </div>
        )

    }
}

export default Card;