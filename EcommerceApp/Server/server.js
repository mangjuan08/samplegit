const express = require("express");
const app = express();
const cors = require("cors");
const mysql = require("mysql");
const bp = require("body-parser");
const multer = require("multer");
const orders = require("./routes/orders/AllAboutOrders");
const products = require("./routes/products/AllAboutProducts");
const category = require("./routes/category/AllAboutCategory");
const users = require("./routes/users/AllAboutUsers");
const morgan = require("morgan");
const fs = require("fs");
const moment = require("moment");
const now = moment();

app.use(cors());
app.use(
  bp.urlencoded({
    extended: true
  })
);

app.use(bp.json({ limit: "20mb", extended: true }));
app.use(bp.urlencoded({ limit: "20mb", extended: true }));

app.use(bp.json());

const port = process.env.PORT || 9000;
app.listen(port);

//routes
app.use("/api/orders", orders);
app.use("/api/products", products);
app.use("/api/category", category);
app.use("/api/user", users);

app.use(morgan("dev"));
