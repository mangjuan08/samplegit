const express = require('express');
const Route = express.Router();
var bp = require("body-parser");
var cors = require("cors");
var db = require("../db/connect");

/*FETCH ALL ORDERS*/
Route.get('/', cors(), function(req, res) {
    db.query(
        "SELECT COUNT(IdOrder) as totalCount, IdOrder,Name,Surname,NameProduct,Stato,Quantita,DateOrder,NameCategory FROM orders O,products P, clients C, categoria CC WHERE O.FkIdProduct=P.IdProduct AND O.FkIdClient=C.IdClient AND CC.IdCategory=P.FkIdCategory GROUP BY IdOrder,Name,Surname,NameProduct,Stato,Quantita,DateOrder,NameCategory ORDER BY IdOrder",
        function(req, results) {
            const a = JSON.stringify(results);
            res.send(a);
        }
    );
})

Route.delete('/deleteOrder', cors(), function(req, res) {

    const IdOrder = req.body.key;
    db.query("DELETE from orders WHERE IdOrder = ?", IdOrder,
        function(req, results, err) {
            if (err)
                throw err;
            res.send("success")
        }

    );
})

Route.post('/insertOrder', cors(), function(req, res) {
    /*
    const name = "";
    let param = {
        name: req.body.values.Name
    }*/

    var ParseValue = JSON.parse(req.body.values);
    console.log("NAME: " + ParseValue.Name + " NameProduct: " + ParseValue.NameProduct)
    let parameter = [
        ParseValue.Name,
        ParseValue.NameProduct
    ]

    var a = "(SELECT IdProduct FROM products WHERE NameProduct= " + ParseValue.NameProduct + ")"
    var b = "(SELECT IdClient FROM clients WHERE Name= " + ParseValue.Name + ")"
        /*INSERT INTO `orders` (`FkIdProduct`, `FkIdClient`, `Stato`, `Quantita`, `DateOrder`) VALUES ((SELECT IdProduct FROM products WHERE NameProduct= ?), (SELECT IdClient FROM clients WHERE Name= ?), '','', NULL);*/
    const query = "INSERT INTO `orders` (FkIdProduct, FkIdClient, Stato, Quantita, DateOrder) VALUES (" + a + ", " + b + ", " + ParseValue.Quantita + ",'', 'NULL');"


    db.query(query,
        function(req, results, err) {
            if (err)
                throw err;
            console.log(err)
            res.send(err)

        }

    );
})

Route.get('/home', cors(), function(req, res) {
    db.query(
        "SELECT * FROM orders cp,products p,clients c,categoria ca WHERE ca.IdCategory = p.FkIdCategory AND cp.FkIdProduct = p.IdProduct AND c.IdClient=cp.FkIdClient",
        function(req, results) {
            const a = JSON.stringify(results);
            res.send(a);
        }
    );
})

Route.put("/updateOrder/:Quantita/:IdProduct/:IdClient", cors(), function(req, res) {
    const Quantita = req.params.Quantita;
    const IdProd = req.params.IdProduct;
    const IdClient = req.params.IdClient;
    let query =
        "CALL UpdateOrders (" + IdProd + "," + Quantita + "," + IdClient + ")";
    db.query(query, (error, results) => {
        if (error) console.log(error);
    });
    res.send("success")
})


module.exports = Route;