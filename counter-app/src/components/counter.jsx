import React, { Component } from 'react';


class Counter extends Component {
    //props are needed to pass information from one component to another
    state = {
        count: 0,
        tags: ['p', 'o', 'g', 'i']
    }

    //instead of creating constructor, with this way it doesn't need anymore to create constructor
    HandleIncrement = () => {
        //instead of creating constructor

        this.setState({ count: this.state.count + 1 });
    }


    HandleDecrement = () => {
        //instead of creating constructor
        if (this.state.count == 0) {
            this.setState({ count: this.state.count = 0 });
        }
        else {
            this.setState({ count: this.state.count - 1 });
        }

    }
    render() {

        return (
            <div>
                <span className="badge badge-primary">{this.state.count}</span>
                <button onClick={this.HandleIncrement}>INCREMENT</button>
                <button onClick={this.HandleDecrement}>DECREMENT</button>
            </div>
        );
    }


}


export default Counter;