import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { Token } from '../datamodel/token.datamodel';



@Injectable({
  providedIn: 'root'
})
export class TokenService {
  token:Token

  constructor(private router:Router) { }
  tok="";

  //save the token in local storage in the browser
  HandleToken(token) {
    localStorage.setItem('token',token.access_token);

    this.NabFunc(token);
  }

  //rawr function
  NabFunc(tok) {
      this.tok=tok;
      console.log(this.tok);
  }
  
 
  //logout
  RemoveToken() {
    localStorage.removeItem('token');
    this.router.navigate(['']);
    
  }

  
}
