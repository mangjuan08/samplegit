import React, { Component } from "react";
import HomePage from "./Home/HomePage";
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link,
  Redirect
} from "react-router-dom";
import ShoppCart from "./ShoppCart/ShoppCart";
import UserRegister from "./User/UserRegister";
import UserLogin from "./User/UserLogin";
import AccountCircleIcon from "@material-ui/icons/AccountCircle";
import PersonAddIcon from "@material-ui/icons/PersonAdd";
import ShoppingCartIcon from "@material-ui/icons/ShoppingCart";
import UserLoggedIn from "./UserLoggedIn/UserLoggedIn";

class MainContainer extends Component {
  constructor() {
    super();
    this.state = {
      Count: 0,
      loggedIn: false,
      token: "",
      error: ""
    };
    this.Login = this.LogIn.bind(this);
    this.Logout = this.Logout.bind(this);
  }

  AddValue = () => {
    this.setState({
      Count: this.state.Count + 1
    });
  };

  ClearCart = () => {
    this.setState({
      Count: 0
    });
  };

  LogIn(isLoggedIn, tok) {
    if (isLoggedIn) {
      this.setState(
        {
          loggedIn: true,
          token: tok
        },
        () => {
          localStorage.setItem("token", this.state.token);
          /*this.history.push("/UserLoggedIn");*/
        }
      );
      alert("LOGGED IN");

      /*this.history.push("/UserLoggedIn");*/
    }
  }
  Logout() {
    this.setState({
      loggedIn: false
    });
    localStorage.removeItem("token");
  }
  render() {
    return (
      <Router>
        <div>
          <nav className="navbar navbar-expand-lg navbar-light bg-light text-white">
            <div className="container">
              <Link className="navbar-brand" to="/">
                SHOPSITE!
              </Link>
              <button
                className="navbar-toggler"
                type="button"
                data-toggle="collapse"
                data-target="#navbarNav"
                aria-controls="navbarNav"
                aria-expanded="false"
                aria-label="Toggle navigation"
              >
                <span className="navbar-toggler-icon"></span>
              </button>
              <div className="collapse navbar-collapse" id="navbarNav">
                <ul className="navbar-nav ml-auto">
                  <li className="nav-item active">
                    <Link className="nav-link" to="/">
                      CATEGORY
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/Register">
                      <PersonAddIcon></PersonAddIcon>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/Login">
                      <AccountCircleIcon></AccountCircleIcon>
                    </Link>
                  </li>
                  <li className="nav-item">
                    <Link className="nav-link" to="/Cart">
                      <ShoppingCartIcon></ShoppingCartIcon>
                      <span className="badge badge-secondary">
                        {this.state.Count}
                      </span>
                    </Link>
                  </li>
                </ul>
              </div>
            </div>
          </nav>

          <Switch>
            <Route
              exact
              path="/"
              render={props => <HomePage {...props} Add={this.AddValue} />}
            ></Route>
            <Route
              exact
              path="/Cart"
              render={props => (
                <ShoppCart
                  {...props}
                  Add={this.AddValue}
                  ClearCart={this.ClearCart}
                  CountProduct={this.state.Count}
                />
              )}
            ></Route>
            <Route
              exact
              path="/Register"
              render={props => <UserRegister {...props} />}
            ></Route>
            <Route
              exact
              path="/Login"
              render={props =>
                this.state.loggedIn ? (
                  <Redirect to="/UserLoggedIn"></Redirect>
                ) : (
                  <UserLogin
                    {...props}
                    login={this.LogIn.bind(this)}
                    token={this.state.token}
                    error={this.state.error}
                  />
                )
              }
            ></Route>
            <Route
              exact
              path="/UserLoggedIn"
              render={props =>
                this.state.loggedIn === false ? (
                  <Redirect to="/"></Redirect>
                ) : (
                  <UserLoggedIn
                    {...props}
                    token={this.state.token}
                    Logout={this.Logout}
                  />
                )
              }
            ></Route>
          </Switch>
        </div>
      </Router>
    );
  }
}

export default MainContainer;
