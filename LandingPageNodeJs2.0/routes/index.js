const express = require('express');
const Route = express.Router();
const { check, validationResult } = require('express-validator');
const fs = require('fs');
const officegen = require('officegen')
const PDFDocument = require('pdfkit');

//index page
Route.get('/', function (req, res) {
    res.render('index');
})



//when user click the button "RICHIEDI DOCUMENTAZIONE" the datas are being validated
//if datas are validated correctly, a file will be created depending on the "scelta" of user
Route.post('/sendData', [
    check('email', 'inserisci un email valida').isEmail(),
    check('email', 'inserisci email').not().isEmpty(),
    check('nome', 'inserisci nome').not().isEmpty(),
    check('cognome', 'inserisci cognome').not().isEmpty(),
    check('numeroTelefono', 'inserisci il numero di telefono').not().isEmpty(),
    check('numeroTelefono', 'inserisci solo numeri').isInt(),
    check('azienda', 'inserisci nome dell azienda').not().isEmpty(),
    check('fileScelta', 'scelta required').not().isEmpty()
], function (req, res) {
    console.log("HEY");


    var nome = req.body.nome;
    var cognome = req.body.cognome;
    var email = req.body.email;
    var numeroTelefono = req.body.numeroTelefono;
    var azienda = req.body.azienda;
    var scelta = req.body.fileScelta;

    //date that is need for naming the file
    let ts = Date.now();
    let date_ob = new Date(ts);
    let date = date_ob.getDate();
    let month = date_ob.getMonth() + 1;
    let year = date_ob.getFullYear();


    const errors = validationResult(req);

    //if there are no errors nothing will happen
    if (!errors.isEmpty()) {
        console.log(errors);

    }

    //if scelta is "PDF" a pdf will be created
    else if (scelta == "PDF") {

        // Create a document
        const doc = new PDFDocument;


        //use to put the name in the name of file of pdf
        // current timestamp in milliseconds


        // prints date & time in YYYY-MM-DD format
        console.log(year + "-" + month + "-" + date);



        // Pipe its output somewhere, like to a file or HTTP response
        // See below for browser usage
        doc.pipe(fs.createWriteStream('pdf/' + nome + '_' + year + '-' + month + '-' + date + '.pdf'));


        doc.moveDown();
        doc.text(`NOME: ${nome}`, {
            width: 410,
            align: 'center',

        }
        );

        doc.moveDown();
        doc.text(`COGNOME: ${cognome}`, {
            width: 410,
            align: 'center'
        }
        );

        doc.moveDown();
        doc.text(`EMAIL: ${email}`, {
            width: 410,
            align: 'center'
        }
        );

        doc.moveDown();
        doc.text(`NUMERO DI TELEFONO: ${numeroTelefono}`, {
            width: 410,
            align: 'center'
        }
        );

        doc.moveDown();
        doc.text(`AZIENDA: ${azienda}`, {
            width: 410,
            align: 'center'
        }
        );


        // Finalize PDF file
        doc.end();


        //once the file is created, the page refresh
        res.render('index');

    }
    else {

        //a doc file will be created if the "scelta" is "DOC"

        // Create an empty Word object:
        let docx = officegen('docx')

        // Officegen calling this function after finishing to generate the docx document:
        docx.on('finalize', function (written) {
            console.log(
                'Finish to create a Microsoft Word document.'
            )
        })

        // Officegen calling this function to report errors:
        docx.on('error', function (err) {
            console.log(err)
        })

        // Create a new paragraph:
        let pObj = docx.createP()


        pObj.addText('NOME:' + nome); // Use pattern in the background.
        pObj = docx.createP()

       
        pObj.addText('EMAIL: ' + email);
        pObj = docx.createP();

        
        pObj.addText('NUMERO TELEFONO: ' + numeroTelefono);
        pObj = docx.createP();

        
        pObj.addText('AZIENDA: ' + azienda);
        pObj = docx.createP();


        // Let's generate the Word document into a file:

        let out = fs.createWriteStream('doc/' + nome + '_' + year + '-' + month + '-' + date + '.docx')


        out.on('error', function (err) {
            console.log(err)
        })

        // Async call to generate the output file:
        docx.generate(out)

        //once the file is created, the page refresh
        res.render('index');

    }

});



//export landing page
module.exports = Route;